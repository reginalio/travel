#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Backend 
import string
import re  #re.search returns None if no position in the string matches the pattern (regex)
import random
import database
from pymongo import MongoClient
import copy

welcomeMessage = 'Hi $name! My name is Star, are you looking for a place to go on holiday?'

starterQuestion = 'currentYear'

result = 'Your result is: '

connect = {}

# '1' represents a question to be answered, '0' represents a question that should not appear given previous answers
# Also stores answers if a question was asked
userInfo = {

		}

def createConnection(id):
	connect[id] = copy.deepcopy(userInfo)

def removeConnection(id):
	try:
		del connect[id]
	except:
		print('Error deleting connection, id: ' + str(id))

def getResult():
	return result

def questions(q):
	return{
		'numPeople': 'How many people are on the trip?',
		'who': 'With whom are you travelling with?',
		'budget': 'What is your budget?',
		'duration': 'How long do you plan to go on holiday for?',
		'flight': 'What is the longest flight duration you are willing to go on?',
		'priority': 'Which of the following are you most willing to splurge on?',
		'weather': 'What kind of weather do you prefer?',
		'when':'When are you planning to go on holiday?',
		'purpose': 'What is the main purpose of your trip?',
		'drive': '路途中你會想楂車嗎？'
		
		'end': 'End of game'
	}.get(q, 'wrong q key')

answerList = {
		'numPeople': ['Only me', '2', '3-4', '5-10', '10+'],
		'who': ['My partner', 'Friends', 'Family', 'Other'],
		'budget': ['I\'m on a tight budget', ],
		'duration': 'How long do you plan to go on holiday for?',
		'flight': 'What is the longest flight duration you are willing to go on?',
		'priority': ['Flight', 'Accommodation', 'Food', '玩樂'],
		'weather': ['Hot'],
		'when': '',
		'purpose': ['To relax', 'To explore new places', 'To shop and eat'],
		'drive': ['Yes', 'No', 'I would not mind']

		
	}

def getAnswerList(q, id):
	l = answerList[q]

	return l

def getQuestion(prevQuestion, prevAnswer, id):

	global result
	# First question of the game
	if prevQuestion == '0':
		q = random.choice(list(connect[id].keys()))
	else:
		connect[id][prevQuestion] = prevAnswer;		# store student's answer
		q = prevQuestion

	

	# Choose next question if q has not been set
	if q == prevQuestion:
		count = 0
		q = random.choice(list(connect[id].keys()))
		while connect[id][q] != '1' and count < 100:
			q = random.choice(list(connect[id].keys()))
			count += 1
		if count == 100:
			q = 'end'
	
	return q

def argmax(iterable):
    return max(enumerate(iterable), key=lambda x: x[1])[0]

def showResult(id):
	

	getSchoolsFromDB()



def checkAnswerValidilty(q, userAnswerLetter, id):
	validity = True
	modifiedAnswerList = getAnswerList(q, id)
	i = len(modifiedAnswerList)
	validAnswers = string.ascii_lowercase[:i+1] + string.ascii_uppercase[:i+1]
	if (str(userAnswerLetter) not in validAnswers) or (len(userAnswerLetter) != 1):
		validity = False
	return validity

def extractAnswer(q, userAnswerLetter, id):
	modifiedAnswerList = getAnswerList(q, id)
	i = len(modifiedAnswerList)
	if str(userAnswerLetter) in string.ascii_lowercase[:i+1]:
		userAnswerNumber = ord(userAnswerLetter) - ord('a')	# covert lowercase to number
	else:
		userAnswerNumber = ord(userAnswerLetter) - ord('A')	# covert uppercase to number
	prevAnswer = modifiedAnswerList[userAnswerNumber]
	print('Your answer is: '+ modifiedAnswerList[userAnswerNumber])
	print('|')
	return prevAnswer

def getSchoolsFromDB(id):
	connection = MongoClient()
	db = connection.starling.destinations
	destinations = database.query(db, connect[id])

	for record in destinations:
		print(record)
		print()

	connection.close()

	return schools







