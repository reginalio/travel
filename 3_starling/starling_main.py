#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ukstudent
import string

def main():
	prevAnswer = '0'
	prevQuestion = '0'

	q = ukstudent.getQuestion(prevQuestion, prevAnswer)
	print(ukstudent.questions(q))

	while(prevQuestion != 'end'):

		for i, l in enumerate(ukstudent.getAnswerList(q)):
			print ("%s. %s, " %(string.ascii_lowercase[i], l))

		userAnswerLetter = raw_input('\nEnter the letter corresponding to your answer: ')
		print('userAnswerLetter = ', (userAnswerLetter))

		# hack to end quiz
		if userAnswerLetter == 'END':
			#prevQuestion = 'end'
			break

		ansIsValid = ukstudent.checkAnswerValidilty(q, userAnswerLetter)
		if ansIsValid:
			prevAnswer = ukstudent.extractAnswer(q, userAnswerLetter)
			prevQuestion = q
			q = ukstudent.getQuestion(prevQuestion, prevAnswer)
		else:
			print('Invalid answer, please enter a letter corresponding to your answer\n')


		print(ukstudent.questions(q))

		if q == 'end':
			break
	
	print(ukstudent.studentInfo)
	print('\n'+ ukstudent.getResult() + '\n')

	ukstudent.showResult()


if __name__ == '__main__':
    main()