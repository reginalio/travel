# from pymongo import MongoClient


# select destination based on requirements
# give a list of destination that most suit the user

def query(db, userInfo):
	#grades wrapper
	competitiveness = []
	if userInfo['grades'] in ['Very well', 'Above average', 'Average']:
		competitiveness.append('selective')
	if userInfo['grades'] in ['Average', 'Barely making it', 'I haven\'t been able to pass the year']:
		competitiveness.append('academy')
	if userInfo['grades'] == 'I haven\'t been able to pass the year':
		competitiveness.append('comprehensive')

	# query
	destinations = db.find({'$and':[
			{'levels':{'$in':userInfo['level']}},
			{'school type':{'$in':userInfo['school funding']}},
			{'school type':{'$in':competitiveness}},
			{'school type':{'$in':userInfo['school gender']}},
			{'school type': 'boarding' if userInfo['boarding'] == 'Yes' else 'day'}
			]})

	### TODO add rank priorities according to fees, speciality or location

	destination_dict = dict((destination['name'], destination) for destination in destinations)
	return destination_dict


# def main():

# 	connection = MongoClient()
# 	db = connection.db.u destination

# 	#example result array
# 	userInfo = {
# 			'currentYear': 'Form 5',
# 			'yearToUK': 'Form 6',
# 			'form5sure': '0',
# 			'ielts': 'No',
# 			'ieltsScore': '0',
# 			'grades': 'Above average',
# 			'oxbridge': 'Yes',
# 			'nationality': 'No',
# 			'school funding': 'independent',
# 			'relatives': 'No',
# 			'boarding': 'Yes',
# 			'gender': 'Female',
# 			'school gender': ['co-ed'],
# 			# 'relatives-yes1': '',
# 			'area': [],
# 			'cities': '1',
# 			'smallTowns': '1',
# 			'countryside': '1',
# 			'living': '1',
# 			'subject': '1', #Science, Arts, Commerce, Other
# 			'level' : ['a-levels'],
# 			'schoolType': '',
# 			'cityScore': 10,
# 			'smallTownScore': 0,
# 			'countrysideScore': 0,
# 			}

#  destination = query(db, userInfo)

# 	if destination.count() == 0:
# 		print('cannot find matching destination')
# 	else:
# 		for record in destination:
# 		# print out the document
# 			print(record['name'] + ',',record['levels'])
# 			print()

# 	connection.close()



# if __name__ == '__main__':
#     main()

