# State Machine pattern using 'if' statements
# to determine the next state.
import string, sys
from State import State
from StateMachine import StateMachine
from MouseAction import MouseAction
# A different subclass for each state:

class Start(State):
    def run(self):
        print("Hi! My name is Star, are you looking for a place to go on holiday?")

    def next(self, input):
        if input == "YES I need a break!":
            return Travelbot.askduration
        return Travelbot.Start

class AskDuration(State):
    def run(self):
        print("How long are you planning to go on holiday for?")

    def next(self, input):
        if input == "1-3 days":
            return Travelbot.waiting
        if input == Travelbot.enters:
            return Travelbot.trapping
        return Travelbot.luring

class Trapping(State):
    def run(self):
        print("Trapping: Closing door")

    def next(self, input):
        if input == MouseAction.escapes:
            return Travelbot.waiting
        if input == MouseAction.trapped:
            return Travelbot.holding
        return Travelbot.trapping

class Travelbot(StateMachine):
    def __init__(self):
        # Initial state
        StateMachine.__init__(self, Travelbot.start)

# Static variable initialization:
Travelbot.start = Start()
Travelbot.askduration = AskDuration()
Travelbot.asktype = AskType()

moves = map(string.strip,
  open("../mouse/MouseMoves.txt").readlines())
Travelbot().runAll(map(MouseAction, moves))