#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

TODO Authentication, error handling
"""

import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import uuid
import ukstudent
import string

from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)

clients = {}

class Client():
    def __init__(self):
        self.prevAnswer = '0'
        self.prevQuestion = '0'
        self.q = ukstudent.starterQuestion
        self.gameStarted = False

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/chatsocket", ChatSocketHandler),
        ]
        settings = dict(
            cookie_secret='b924425e-c6cf-4137-b05d-eaa80a2614c3',
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
        )
        super(Application, self).__init__(handlers, **settings)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html", messages=ChatSocketHandler.cache)

class ChatSocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200

    def get_compression_options(self):
        # Non-None enables compression with default options.
        return {}

    def open(self, *args):
        # self.id = self.get_argument("Id")
        self.id = uuid.uuid4()
        clients[self.id] = Client()

        ChatSocketHandler.waiters.add(self)
        self.send_message(ukstudent.welcomeMessage)

    def on_close(self):
        ChatSocketHandler.waiters.remove(self)
        ukstudent.removeConnection(self.id)

    def send_message(self, msg):
        message = {
            "id": str(uuid.uuid4()),
            "body": msg,
        }
        message["html"] = tornado.escape.to_basestring(self.render_string("message.html", message=message))
        # ChatSocketHandler.update_cache(message)
        # ChatSocketHandler.send_updates(message)

        try:
            self.write_message(message)
        except:
            logging.error("Error sending message", exc_info=True)

    # @classmethod
    # def update_cache(cls, chat):
    #     cls.cache.append(chat)
    #     if len(cls.cache) > cls.cache_size:
    #         cls.cache = cls.cache[-cls.cache_size:]

    # @classmethod
    # def send_updates(cls, chat):
    #     logging.info("sending message to %d waiters", len(cls.waiters))
    #     # for waiter in cls.waiters:
    #     #     try:
    #     #         waiter.write_message(chat)
    #     #     except:
    #     #         logging.error("Error sending message", exc_info=True)

    # print question and answer set
    def print_question(self):
        self.send_message(ukstudent.questions(clients[self.id].q))

        if clients[self.id].q == 'end':
            logging.info('End of game')
        else:
            # print answer options in format "a. AnswerOne, b. AnswerTwo, c. AnswerThree, ..."
            ans = 'a. ' + ukstudent.getAnswerList(clients[self.id].q, self.id)[0]
            for i, l in enumerate(ukstudent.getAnswerList(clients[self.id].q, self.id)[1:]):
                ans += ', ' + string.ascii_lowercase[i+1] + '. ' + l
            self.send_message(ans)

    def on_message(self, answer):
        logging.info("got message %r", answer)
        parsedAnswer = tornado.escape.json_decode(answer)

        # print user input
        self.send_message(">> " + parsedAnswer["body"])
 
        if clients[self.id].gameStarted == False and parsedAnswer["body"] == 'START':
            clients[self.id].gameStarted = True
            logging.info('game started')
            ukstudent.createConnection(self.id)
            self.print_question()  # print first question

        elif clients[self.id].gameStarted == False and parsedAnswer["body"] != 'START':
            self.send_message('Please enter START to begin the game\n')

        elif clients[self.id].gameStarted == True:
            # check if user input is valid, store user input and proceed to next question if answer valid
            ansIsValid = ukstudent.checkAnswerValidilty(clients[self.id].q, parsedAnswer["body"], self.id)
            if ansIsValid:
                clients[self.id].prevAnswer = ukstudent.extractAnswer(clients[self.id].q, parsedAnswer["body"], self.id)
                clients[self.id].prevQuestion = clients[self.id].q
                clients[self.id].q = ukstudent.getQuestion(clients[self.id].prevQuestion, clients[self.id].prevAnswer, self.id)
                logging.info('Next question: ' + clients[self.id].q)
                if clients[self.id].q == 'end':
                    self.end_game()
            else:
                self.send_message('(!) Invalid answer, please enter a single letter corresponding to your answer\n')

            self.print_question()

    def end_game(self):
        schools = ukstudent.getSchoolsFromDB(self.id)
        if len(schools) == 0:
            self.send_message('Cannot find matching schools')
        else:
            self.send_message('You should consider the following schools... \n')
            for record in schools:
                logging.info(record)
                self.send_message(record)


def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
