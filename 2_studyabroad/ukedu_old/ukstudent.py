#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Backend 
import string
import re  #re.search returns None if no position in the string matches the pattern
import random


welcomeMessage = 'You want to study in the UK but don\'t know where? We can help!\n'
welcomeMessage += 'Answer the following questions and we will find the most suitable schools/universities for you. \n'
welcomeMessage += 'To start, enter START'

starterQuestion = 'currentYear'

result = 'Your result is: '

# GCSE, A-level, Foundation, University

# '1' represents a question to be answered, '0' represents a question that should not appear given previous answers
# Also stores answers if a question was asked
studentInfo = {
		'currentYear': '1',
		'yearToUK': '1',
		'form5sure': '0',
		'ielts': '1',
		'ieltsScore': '0',
		'grades': '1',
		'nationality': '1',
		'relatives': '1',
		# 'relatives-yes1': '',
		'area': [],
		'cities': '1',
		'smallTowns': '1',
		'countryside': '1',
		'living': '1',
		'subject': '1', #Science, Arts, Commerce, Other
		'private': '0', # only for EU students
		'level' : '0',
		'schoolType': '',
		'cityScore': 0,
		'smallTownScore': 0,
		'countrysideScore': 0,
		}

def getResult():
	return result

def questions(q):
	return{
		'currentYear': 'Which year are you in now?',
		'yearToUK': 'Which year are you planning to go to the UK?',
		'form5sure': 'Most students go to the UK to study Form 4 (2 years GCSE) or Form 6 (2 years A-level), going to the UK to study Form 5 may mean you will have to retake or complete the 2-year GCSE in one year. Are you sure you want to go to the UK to study Form 5?',
		'ielts': 'Have you taken IELTs before?',
		'ieltsScore': 'What was your best ielts score?',
		'grades': 'How well do you perform in school?',
		'nationality': 'Do you have an EU passport?',
		'relatives': 'Do you have any relatives in the UK that you can stay with during term time?',
		'area': 'Do you prefer living in cities, small towns or the countryside?',
		'cities': 'Do you want to live in big cities?',
		'smallTowns': 'Would you like to live in small towns?',
		'countryside': 'Do you like living in the countryside?',
		'living': 'What is more important for you? Easy access to places or a slower pace of life',
		'subject': 'Which subject area are you planning to study in the future?', #Science, Arts, Commerce, Other
		'private': 'Will you consider studying in a private school?', # only for EU students
		'end': 'End of game'
	}.get(q, 'wrong q key')

# def answerList_(a):
# 	return{
# 		'currentYear': ['Form 3', 'Form 4', 'Form 5', 'Form 6'],
# 		'yearToUK': ['Form 4', 'Form 5', 'Form 6', 'University', 'I am not sure'],
# 		'ielts': ['Yes', 'Yes but I am retaking it', 'No but I am taking it soon', 'No'],
# 		'ieltsScore': ['7 or above', '6 or 6.5', '5.5', '5', 'Below 5'],
# 		'grades': ['Very well', 'Above average', 'Average', 'Barely making it', 'I haven\'t been able to pass the year'],
# 		'nationality': ['Yes', 'No'],
# 		'relatives': ['Yes', 'No'],
# 		'area': ['Cities', 'Small towns', 'Countryside'],
# 		'cities': ['Yes, I love it', 'Yes, it is ok', 'No I\'d prefer not', 'No absolutely not'],
# 		'smallTowns': ['Yes', 'No, it\'s not exciting enough', 'No, I prefer even quieter places'],
# 		'countryside': ['Yes', 'No'],
# 		'living': ['Easy access to places', 'Slower pace of life'],
# 		'subject': ['Sciences', 'Arts', 'Commerce', 'Others'],
# 		'private': ['Yes', 'No']
# 	}.get(a, 'wrong a key')

answerList = {
		'currentYear': ['Form 3', 'Form 4', 'Form 5', 'Form 6'],
		'yearToUK': ['Form 4', 'Form 5', 'Form 6', 'Foundation/University', 'I am not sure'],
		'form5sure': ['Yes, I am sure', 'No, I probably want to go to study Form 4', 'No, I probably want to go to study Form 6'],
		'ielts': ['Yes', 'Yes but I am retaking it', 'No but I am taking it soon', 'No'],
		'ieltsScore': ['7 or above', '6 or 6.5', '5.5', '5', 'Below 5'],
		'grades': ['Very well', 'Above average', 'Average', 'Barely making it', 'I haven\'t been able to pass the year'],
		'nationality': ['Yes', 'No'],
		'relatives': ['Yes', 'No'],
		'area': ['Cities', 'Small towns', 'Countryside'],
		'cities': ['Yes, I love it', 'Yes, it is ok', 'No I\'d prefer not', 'No absolutely not'],
		'smallTowns': ['Yes', 'No, it\'s not exciting enough', 'No, I prefer even quieter places'],
		'countryside': ['Yes', 'No'],
		'living': ['Easy access to places', 'Slower pace of life'],
		'subject': ['Sciences', 'Arts', 'Commerce', 'Others'],
		'private': ['Yes', 'No']
	}

def getAnswerList(q):
	return answerList[q]

def getQuestion(prevQuestion, prevAnswer):

	global result
	# First question of the game
	if prevQuestion == '0':
		q = 'currentYear';
	else:
		studentInfo[prevQuestion] = prevAnswer;		# store student's answer
		q = prevQuestion

	# studentInfo.update(studentInfo.fromkeys(prevQuestion, prevAnswer))

	if prevQuestion == 'currentYear':
		q = 'yearToUK'
		# if prevAnswer == 'Form 3':
		if prevAnswer == 'Form 4':
			studentInfo.update(studentInfo.fromkeys(['ielts', 'ieltsScore'], '0'))
			del answerList['yearToUK'][0]
		elif prevAnswer == 'Form 5':
			del answerList['yearToUK'][0:2]
		elif prevAnswer == 'Form 6':
			studentInfo.update(studentInfo.fromkeys(['private', 'yearToUK', 'relatives', 'nationality'], '0'))
			studentInfo['yearToUK'] = 'Foundation/University'
			q = 'grades'


	if prevQuestion == 'yearToUK':
		if prevAnswer == 'Form 4':
			studentInfo.update(studentInfo.fromkeys(['ielts', 'ieltsScore'], '0'))
			# studentInfo.update(studentInfo.fromkeys(['private'], '1'))
			result += 'boarding schools. '
			studentInfo['level'] = 'GCSE'
		elif prevAnswer == 'Form 5':
			if studentInfo['currentYear'] == 'Form 3' or studentInfo['currentYear'] == 'Form 4':
				q = 'form5sure'
				if studentInfo['currentYear'] == 'Form 4':
					del answerList['form5sure'][1]

		elif prevAnswer == 'Foundation/University':
			studentInfo.update(studentInfo.fromkeys(['private', 'relatives','nationality'], '0'))
			if studentInfo['currentYear'] == 'Form 3' or studentInfo['currentYear'] == 'Form 4':
				result += ' To apply to UK universities, you will need GCE A-level results. You can take A-level examination in Macau or in the UK.' 
			#result += u'中文 '.encode('utf-8')
		elif prevAnswer == 'I am not sure':
			if studentInfo['currentYear'] == 'Form 3':
				result += ' People go to the UK to study GCSE in Year 10 (Form 4) and A-Level in Year 12 (Form 6) most often.'
				studentInfo['level'] = 'GCSE or A-Levels'
				studentInfo['schoolType'] += 'boarding schools'
			elif studentInfo['currentYear'] == 'Form 4' or studentInfo['currentYear'] == 'Form 5':
				result += ' We suggest you to go to the UK to study A-level in Year 12 (Form 6). '
				result += ' If you would like to go to UK Univerities you will need A-level grades (obtained in UK/Macau). '
				studentInfo['level'] = 'A-Levels'
				studentInfo['schoolType'] += 'boarding schools'

			q = 'grades'

	if prevQuestion == 'form5sure':
		if prevAnswer == 'Yes, I am sure':
			studentInfo['yearToUK'] = 'Form 5'
		elif prevAnswer == 'No, I probably want to go to study Form 4':
			studentInfo['yearToUK'] = 'Form 4'
		elif prevAnswer == 'No, I probably want to go to study Form 6':
			studentInfo['yearToUK'] = 'Form 6'

	if prevQuestion == 'ielts':
		if prevAnswer == 'Yes' or prevAnswer == 'Yes but I am retaking it':
			studentInfo.update(studentInfo.fromkeys(['ieltsScore'], '1'))
			q = 'ieltsScore'
		elif prevAnswer == 'No' and (studentInfo['yearToUK'] == 'Form 6' or studentInfo['yearToUK'] == 'Foundation/University'):
			result += ' You will need to take  IELTs before applying to UK Universities/Foundation courses.'
			# studentInfo.update(studentInfo.fromkeys(['ieltsScore'], '0'))

	if prevQuestion == 'nationality':
		if prevAnswer == 'Yes':
			result += ' You can consider public schools. '
			studentInfo.update(studentInfo.fromkeys(['private'], '1'))
			studentInfo['schoolType'] += 'public ' + studentInfo['schoolType']
		if prevAnswer == 'No':		#no EU passport - does not have a choice of public/private schools
			studentInfo.update(studentInfo.fromkeys(['private'], '0'))
			studentInfo['schoolType'] += 'private ' + studentInfo['schoolType']


	if prevQuestion == 'private':
		if prevAnswer == 'No':
			if studentInfo['nationality'] == 'No':
				result += ' Without a EU passport you may need to consider private schools.'
			else:
				result += ' Public schools. '
		else:
			studentInfo['schoolType'] = 'private and' + studentInfo['schoolType']


	if prevQuestion == 'grades':
		if studentInfo['yearToUK'] == 'Form 5':
			studentInfo['schoolType'] += 'boarding schools'
			if prevAnswer == 'Very well':
				result += ' With good grades, you may consider doing a 1-year GCSE or jump directly to Year 12 A-level courses. '
				studentInfo['level'] = '1-year GCSE or A-levels course'
			elif prevAnswer == 'Above average' or prevAnswer == 'Average':
				result += ' With good grades, you may consider doing a 1-year GCSE. '
				studentInfo['level'] = '1-year GCSE course'
			elif prevAnswer == 'Barely making it' or prevAnswer == 'I haven\'t been able to pass the year':
				result += ' You may consider retaking Year 10 in the UK and do the GCSE course. '
				studentInfo['level'] = '2-year GCSE course'
				
		elif studentInfo['yearToUK'] == 'Form 6':
			studentInfo['schoolType'] += 'boarding schools'
			if prevAnswer == 'Very well' or prevAnswer == 'Above average':
				result += ' You should consider doing A-levels before University. < Recommend best A-level schools>'
				studentInfo['level'] = 'A-level course'
			elif prevAnswer == 'Average':
				result += ' You should consider doing A-levels before University or apply to University Foundation courses.'
				result += ' < Recommend good A-level schools and colleges>'
				studentInfo['level'] = 'A-level'
			elif prevAnswer == 'Barely making it' or prevAnswer == 'I haven\'t been able to pass the year':
				result += ' Perhaps you can retake GCSE in one year. '
				studentInfo['level'] = '1-year GCSE course'
		elif studentInfo['yearToUK'] == 'Foundation/University':
			if prevAnswer == 'Very well' or prevAnswer == 'Above average' or prevAnswer == 'Average':
				studentInfo['level'] = 'University'
				studentInfo['schoolType'] += 'University'
				result += ' Universities.'
			elif prevAnswer == 'Barely making it' or prevAnswer == 'I haven\'t been able to pass the year':
				result += ' Foundation. '
				studentInfo['level'] = 'Foundation'
				studentInfo['schoolType'] += 'certain universities'
		# elif studentInfo['yearToUK'] == 'I am not sure':



		# elif studentInfo['yearToUK'] == 'I am not sure':
		# 	if studentInfo['currentYear'] == ''
	if prevQuestion == 'relatives':
		if prevAnswer == 'Yes':
			studentInfo['schoolType'] = 'non-boarding or ' + studentInfo['schoolType']

	if prevQuestion == 'cities':
		if prevAnswer == 'Yes, I love it':
			result += ' Big cities: London, Manchester '
			studentInfo['area'].append('cities')
			studentInfo['cityScore'] += 5
		elif prevAnswer == 'Yes, it is ok':
			studentInfo['cityScore'] += 3
		elif prevAnswer == 'No I\'d prefer not':
			studentInfo['cityScore'] -= 3
		elif prevAnswer == 'No absolutely not':
			studentInfo['cityScore'] -= 5

	if prevQuestion == 'smallTowns':
		if prevAnswer == 'Yes':
			studentInfo['area'].append('small towns')
			studentInfo['smallTownScore'] += 5
		elif prevAnswer == 'No, it\'s not exciting enough':
			studentInfo['area'].append('cities')
			studentInfo['smallTownScore'] -= 3
			studentInfo['cityScore'] += 3
			studentInfo.update(studentInfo.fromkeys(['cities', 'countryside'], '0'))
		elif prevAnswer == 'No, I prefer even quieter places':
			studentInfo['area'] == 'Countryside'
			studentInfo['countrysideScore'] += 3
			studentInfo.update(studentInfo.fromkeys(['cities', 'countryside'], '0'))

	if prevQuestion == 'countryside':
		if prevAnswer == 'Yes':
			studentInfo['countrysideScore'] += 5
		elif prevAnswer == 'No':
			studentInfo['countrysideScore'] -= 5

	if prevQuestion == 'living':
		if prevAnswer == 'Easy access to places':
			studentInfo['cityScore'] += 5
			studentInfo['smallTownScore'] += 2
			studentInfo['countrysideScore'] -= 5
		elif prevAnswer == 'Slower pace of life':
			studentInfo['cityScore'] -= 5
			studentInfo['smallTownScore'] += 2
			studentInfo['countrysideScore'] += 5


	# Choose next question if q has not been set
	if q == prevQuestion:
		count = 0
		q = random.choice(list(studentInfo.keys()))
		while studentInfo[q] != '1' and count < 100:
			q = random.choice(list(studentInfo.keys()))
			count += 1
		if count == 100:
			q = 'end'
	
	return q

def argmax(iterable):
    return max(enumerate(iterable), key=lambda x: x[1])[0]

def showResult():
	r = 'You are looking to go to the UK'
	if studentInfo['yearToUK'] != 'I am not sure' and studentInfo['yearToUK'] != 'Foundation/University':
		r += ' in '
		r += studentInfo['yearToUK']

	r += ' to study '
	if studentInfo['yearToUK'] != 'I am not sure':
		r += studentInfo['level']

	r += ' \n '

	if studentInfo['nationality'] != '0':
		r += 'Since you '
		if studentInfo['nationality'] == 'Yes':
			r += 'have '
			pp = ' public and private, '
		else:
			r += 'do not have '
			pp = ' private, '
		r += 'an EU passport, and you '
		if studentInfo['relatives'] == 'Yes':
			r += 'have '
			bs = ' boarding and non-boarding school'
		else:
			r += 'do not have '
			bs = ' boarding school'
		r += 'relatives in the UK, you can consider '
		r += pp + bs


	r += ' \n '

	i = argmax([studentInfo['cityScore'], studentInfo['smallTownScore'], studentInfo['countrysideScore']])

	areaResult = ['You like cities', 'You like small towns', 'You like countryside']
	areas = ['cities', 'small towns', 'countryside']

	r += areaResult[i]
	r += ' <suggest '
	r += studentInfo['schoolType']
	r += ' in '
	r += areas[i]
	r += '>'


	print(r)

def checkAnswerValidilty(q, userAnswerLetter):
	i = len(answerList[q])
	validAnswers = string.ascii_lowercase[:i+1] + string.ascii_uppercase[:i+1]
	if (str(userAnswerLetter) not in validAnswers) or (len(userAnswerLetter) > 1) :
		return False
	else:
		return True

def extractAnswer(q, userAnswerLetter):
	i = len(answerList[q])
	if str(userAnswerLetter) in string.ascii_lowercase[:i+1]:
		userAnswerNumber = ord(userAnswerLetter) - ord('a')	# covert lowercase to number
	else:
		userAnswerNumber = ord(userAnswerLetter) - ord('A')	# covert uppercase to number
	prevAnswer = answerList[q][userAnswerNumber]
	print('Your answer is: ',answerList[q][userAnswerNumber])
	print('|')
	return prevAnswer
