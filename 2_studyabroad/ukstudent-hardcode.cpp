#include <iostream>


using namespace std;

class Student
{
public:
	int currentYear;
	int expectedYearToUK;
	float ieltsScore; //ieltsScore = 0 means the student has not taken ielts

	Student();
	~Student();
	
};

Student::Student() : ieltsScore(0){}
Student::~Student(){}

int main(){

	Student s;
	char tmp1;
	char tmp2;

	cout << " ----------------------------------------------------------\n ";
	cout << "||        You want to study in the UK? Let me help!      ||\n";
	cout << " ----------------------------------------------------------\n ";

	while (! (tmp1 == 'a' || tmp1 == 'b' || tmp1 == 'c' || tmp1 == 'd' || tmp1 == 'e' ){
		cout << "Which year are you in at the moment? \n";
		cout << "a: Form 2, b: Form 3, c: Form 4, d: Form 5, e: Form 6\n" <<
			"(Enter a single letter from a-e please)" << endl;


		cin >> tmp1;
	}
	switch (tmp1){
		case('a'): s.currentYear = 2; break;
		case('b'): s.currentYear = 3; break;
		case('c'): s.currentYear = 4; break;
		case('d'): s.currentYear = 5; break;
		case('e'): s.currentYear = 6; break;
		default: cout << "Please enter a single letter from a-e" << endl;
	}

	// debug msg
	cout << "s.currentYear is " << s.currentYear << endl;


	if (s.currentYear != 6){
		cout << "Which year would you like to go to the UK? \n";
		cout << "a: Form 3, b: Form 4, c: Form 5, d: Form 6, e: University, f: I don't know" << endl;
	}

		cout << "Do you know which year you would like to go to the UK? \n";
		cout << "a: Yes, b: No" << endl;

		cin >> tmp1;
	


	switch (tmp1){
	case('a'): 	cout << "Which year would you like to go to the UK? \n";
				cout << "a: Form 3, b: Form 4, c: Form 5, d: Form 6, e: University" << endl;

				cin >> tmp2;

				switch (tmp2){
				case('a'): s.expectedYearToUK = 3; break;
				case('b'): s.expectedYearToUK = 4; break;
				case('c'): s.expectedYearToUK = 5; break;
				case('d'): s.expectedYearToUK = 6; break;
				case('e'): s.expectedYearToUK = 7; break;
				cout << "Please enter a single letter from a-e" << endl;
				}

				 break;

	case('b'): 	s.expectedYearToUK = 0; break;	// doesn't know which year to go to UK
	default: 	cout << "Please enter a single letter from a-b" << endl;
	}


	// ielts neccessary only if not taking GCSE english
	cout << "Have you taken IELTS? \n";
	cout << "a: Yes, b: Yes, but I am retaking it soon, c: No, but I am taking it soon, d: No" << endl;

	cin >> tmp1;

	switch(tmp1){
		case('a'):	cout << "Enter your IELTS score --> ";
					cin >> s.ieltsScore;
					break;
		case('b'):	cout << "Enter your best-to-date IELTS score -->";
					cin >> s.ieltsScore;
					break;
		// do nothing for c
		case('d'):	if (s.expectedYearToUK != 3 && s.expectedYearToUK != 4){
						cout << "You should consider taking IELTS unless you have already achieved a B or better in GCSE English." << endl;

					}
					break;
		default:	cout << "Please enter a single letter from a-d" << endl;
	}

	cout << "How well do you perform in school?\n";
	cout << "a: Very well, b: Above average, c: Average, d: Barely making it, e: I haven't been able to pass the year" << endl;

	cin >> tmp1;





	cout << " End of Test" << endl;

}